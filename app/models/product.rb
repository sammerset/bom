class Product < ActiveRecord::Base
  monetize :price_cents, :allow_nil => false,
    :numericality => {
      :greater_than_or_equal_to => 0,
      :less_than_or_equal_to => 100000
    }

  validates :name, presence: true, length: { maximum: 250 }
  validates :description, presence: true, length: { maximum: 1000 }
  
end
