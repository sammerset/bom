FactoryGirl.define do

  factory :product, class: Product do
    name "Flashlight"
    description "Very good flashlight"
    price_cents 2000
  end

  factory :product_2, class: Product do
    name "Flashlight Sakuro"
    description "Very good flashlight Sakuro"
    price_cents 20000
  end

end