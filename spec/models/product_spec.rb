require 'rails_helper'

describe Product do

  before(:each) do
    create(:product)
  end

  it { should validate_presence_of(:name).with_message('can\'t be blank')}
  it { should validate_presence_of(:description).with_message('can\'t be blank')}
  it { should validate_presence_of(:price_cents).with_message('is not a number')}
  it { should ensure_length_of(:name).is_at_most(250).with_message('is too long (maximum is 250 characters)')}
  it { should ensure_length_of(:description).is_at_most(1000).with_message('is too long (maximum is 1000 characters)')}
  it { should validate_numericality_of(:price_cents) }

  it "creation product" do
    expect(Product.last.name).to eq("Flashlight")
  end

end

describe ProductsController, :type => :controller do

  describe "GET #index" do
    it "responds successfully with an HTTP 200 status code" do
      get :index
      expect(response).to be_success
      expect(response).to have_http_status(200)
    end

    it "renders the index template" do
      get :index
      expect(response).to render_template("index")
    end

    it "loads all of the products into @products" do
      product1, product2 = create(:product), create(:product_2)
      get :index

      expect(assigns(:products)).to match_array([product1, product2])
    end
  end

  describe "POST #create" do

    context "with valid attributes" do 
      it "creates a new contact" do 
        expect{ post :create, product: attributes_for(:product) }.to change(Product,:count).by(1) 
      end 

      it "redirects to the new contact" do 
        post :create, product: attributes_for(:product) 
	      response.should redirect_to products_url 
	    end 
	  end

      context "with invalid attributes" do 
        it "does not save the new contact" do 
          expect{ post :create, product: attributes_for(:product, name: nil) }.to_not change(Product,:count) 
        end 

        it "re-renders the new method" do 
          post :create, product: attributes_for(:product, name: nil) 
          response.should render_template :index 
        end 
      end

    end



    describe 'PUT #update' do 
      before :each do 
        @product = create(:product) 
      end 

      context "valid attributes" do 
        it "located the requested @product" do 
          put :update, id: @product, product: attributes_for(:product) 
          assigns(:product).should eq(@product) 
        end 

        it "changes @product's attributes" do 
          put :update, id: @product, product: attributes_for(:product, name: "Ball", description: "red_ball", price_cents: 12000) 
          @product.reload 
          @product.name.should eq("Ball") 
          @product.description.should eq("red_ball") 
          @product.price_cents.should eq(12000) 
        end 

        it "redirects to the updated product" do 
          put :update, id: @product, product: attributes_for(:product) 
          response.should redirect_to @product 
        end 
      end 

      context "invalid attributes" do 
        it "locates the requested @product" do 
          put :update, id: @product, product: attributes_for(:product, name: nil) 
          assigns(:product).should eq(@product) 
        end 

      it "does not change @product's attributes" do 
        put :update, id: @product, product: attributes_for(:product, name: "LLoyd", description: "ball_red", price_cents: 12000000) 
        @product.reload 
        @product.name.should_not eq("LLoyd") 
        @product.description.should eq("Very good flashlight")
        @product.price_cents.should eq(2000) 
      end 

      it "re-renders the edit method" do 
        put :update, id: @product, product: attributes_for(:product, name: nil) 
        response.should render_template :edit
      end 
    end 

    describe 'DELETE #destroy' do 
	  before :each do 
	    @product = create(:product) 
	  end 

	  it "deletes the product" do 
	    expect{ delete :destroy, id: @product }.to change(Product,:count).by(-1) 
	  end 

	  it "redirects to contacts#index" do 
	    delete :destroy, id: @product 
	    response.should redirect_to products_url 
	  end 
	end

  end
  
end
